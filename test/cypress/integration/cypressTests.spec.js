import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';
const PreBuiltWizardJob0Data = require('../fixtures/stubs/Pre-Built Wizard Job0.json');
const PreBuiltWizardJob1Data = require('../fixtures/stubs/Pre-Built Wizard Job1.json');
const PreBuiltWizardReDiscoverJob2Data = require('../fixtures/stubs/Pre-Built Wizard Re-Discover Job2.json');
const PreBuiltWizardReDiscoverJob3Data = require('../fixtures/stubs/Pre-Built Wizard Re-Discover Job3.json');

function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });
    // Check if Stub flag is enabled
    if(isStub){
      stubTasks.forEach(stubTask=>{
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }
    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();

  return workflowRunner;
}

// Function to delete the stubbed workflow and reimport it without the stub tasks
function replaceStubTasks(workflowRunner, workflowName) {
    workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: workflowName });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
}

describe('Default: Cypress Tests', function () {
  let prebuiltRunner;
  let PreBuiltWizardJob0Workflow;
  let PreBuiltWizardJob1Workflow;
  let PreBuiltWizardReDiscoverJob2Workflow;
  let PreBuiltWizardReDiscoverJob3Workflow;
  
  before(function () {
    //creates a prebuilt runner for importing the prebuilt
    cy.fixture(`../../../artifact.json`).then((data) => {
      prebuiltRunner = new PrebuiltRunner(data);
    });
    cy.fixture(`../../../bundles/workflows/Pre-Built Wizard.json`).then((data) => {
      PreBuiltWizardJob0Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Pre-Built Wizard.json`).then((data) => {
      PreBuiltWizardJob1Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Pre-Built Wizard Re-Discover.json`).then((data) => {
      PreBuiltWizardReDiscoverJob2Workflow = data;
    });
    cy.fixture(`../../../bundles/workflows/Pre-Built Wizard Re-Discover.json`).then((data) => {
      PreBuiltWizardReDiscoverJob3Workflow = data;
    });
    
  });

  after(function() {
    prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
  });

  describe('Default: Imports Pre-Built', function () {
    // eslint-disable-next-line mocha/no-hooks-for-single-case
    it('Default: Should import the prebuilt into IAP', function () {
        prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
        prebuiltRunner.importPrebuilt.single({});
    });
  })

  describe('Pre-Built Wizard', function() {
    it('It should create Test Pre-Built with scope @test, version 0.0.1, and contributor Itential Test given Operations Manager automation entrypoint', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PreBuiltWizardJob0Workflow, importWorkflow, isStub, PreBuiltWizardJob0Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PreBuiltWizardJob0Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PreBuiltWizardJob0Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PreBuiltWizardJob0Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PreBuiltWizardJob0Workflow);
      });
    })
  })

  describe('Pre-Built Wizard', function() {
    it('It should create Test Pre-Built with scope @test, version 0.0.1, and contributor Itential Test given workflow entrypoint', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PreBuiltWizardJob1Workflow, importWorkflow, isStub, PreBuiltWizardJob1Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PreBuiltWizardJob1Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PreBuiltWizardJob1Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PreBuiltWizardJob1Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PreBuiltWizardJob1Workflow);
      });
    })
  })

  describe('Pre-Built Wizard Re-Discover', function() {
    it('It should re-discovery Test Pre-Built as is with no changes to Pre-Built', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PreBuiltWizardReDiscoverJob2Workflow, importWorkflow, isStub, PreBuiltWizardReDiscoverJob2Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PreBuiltWizardReDiscoverJob2Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PreBuiltWizardReDiscoverJob2Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PreBuiltWizardReDiscoverJob2Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PreBuiltWizardReDiscoverJob2Workflow);
      });
    })
  })

  describe('Pre-Built Wizard Re-Discover', function() {
    it('It should re-discovery Test Pre-Built with additional Operation Manager automation', function () {
      const importWorkflow = true;
      const isStub = true;
      // create the job runner so it can be used in future tests
      const workflowRunner = initializeWorkflowRunner(PreBuiltWizardReDiscoverJob3Workflow, importWorkflow, isStub, PreBuiltWizardReDiscoverJob3Data.stubTasks);
      // this has to be customized to each IAP version.

      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: PreBuiltWizardReDiscoverJob3Data.input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['status']).eql(PreBuiltWizardReDiscoverJob3Data.expectedTaskResults.status);
        workflowRunner.job.getJobVariables(jobVariableResults._id).then(jobVariables => {
          delete jobVariables._id;
          delete jobVariables.initiator;
          expect(jobVariables).eql(PreBuiltWizardReDiscoverJob3Data.expectedTaskResults.variables);
        });
        /* Restore the workflow without the stub tasks */
        replaceStubTasks(workflowRunner, PreBuiltWizardReDiscoverJob3Workflow);
      });
    })
  })
});
