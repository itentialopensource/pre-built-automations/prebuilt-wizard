# Pre-Built Wizard Re-Discovery

## Table of Contents

- [Pre-Built Wizard Re-Discovery](#pre-built-wizard-re-discovery)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Getting Started](#getting-started)
    - [Supported IAP Versions](#supported-iap-versions)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Workflow Project](#using-this-workflow-project)
    - [Entry Point IAP Component](#entry-point-iap-component)
    - [Inputs](#inputs)
    - [Outputs](#outputs)
    - [Query Output](#query-output)
    - [Example Inputs and Outputs](#example-inputs-and-outputs)
    - [API Links](#api-links)
  - [Support](#support)

## Overview

Pre-Built Wizard Re-Discovery provides the ability to update an existing Pre-Built in Admin Essentials with any new or removed IAP components for a particular use case or automation

Capabilities include:
- Take in the name of the Pre-Built installed in Admin Essentials and update it with any new or removed IAP components for an existing network use case or automation



### Configuring Dependencies
  
#### App-Artifacts

Users need to install App-Artifacts to be able to run Pre-Built Wizard. If you do not currently have App-Artifacts installed as a service on your node, the .tgz file or "tarball" can be obtained from the <a href='https://registry.aws.itential.com/#browse/browse:app-prebuilt_automations' target='_blank'>Nexus repository</a>. This webpage requires an active account with Itential.

Please use your Itential Customer Success account if you need support for this or do not have credentials for Nexus. After downloading the latest App-Artifacts app, please refer to the instructions included in the App-Artifacts README to install it.
  



### Running Pre-Built Wizard Re-Discovery to Update A Pre-Built Bundle

If there are components that are no longer included in the Pre-Built that need to be recreated with Pre-Built Wizard Re-Discovery, be sure to remove them when interacting with the first manual task shown during runtime of the job. Further, if there are any components that were manually added to the Pre-Built, ensure that those components are still present in this manual task.

After updating IAP components from which to discover any related IAP components, the discovered IAP components will be shown in a second manual task. On that being accepted, Pre-Built Wizard Re-Discovery will update the existing Pre-Built in Admin Essentials with the newly created Pre-Built.

## Getting Started

### Supported IAP Versions

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **Pre-Built Wizard Re-Discovery** has been tested with:


- IAP **2023.2**



### External Dependencies

This version of **Pre-Built Wizard Re-Discovery** has been tested with:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>App-Artifacts</td>
      <td></td>
      <td>^6.5.5-2023.2.0</td>
    </tr>
  </tbody>
</table>





### Adapters

No adapters required to run **Pre-Built Wizard Re-Discovery**.


### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Workflow Project.
- Import the Workflow Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).

### Testing

While Itential tests this Workflow Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Workflow Project into a development/testing environment in which you can test the Workflow Project.

## Using this Workflow Project


### Entry Point IAP Component

The primary IAP component to run **Pre-Built Wizard Re-Discovery** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Pre-Built Wizard Re-Discover</td>
      <td>Operations Manager Automation</td>
    </tr>
  </tbody>
</table>

### Inputs

The following table lists the inputs for **Pre-Built Wizard Re-Discovery**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>prebuilt</td>
      <td>string</td>
      <td>yes</td>
      <td>Name of an existing Pre-Built installed in Admin Essentials</td>
      <td><pre lang="json">@scope/pre-built-name</pre></td>
    </tr>
  </tbody>
</table>



### Outputs

There are no outputs for **Pre-Built Wizard Re-Discovery**.


### Query Output

There are no query output examples for **Pre-Built Wizard Re-Discovery**.




### Example Inputs and Outputs

  
#### Example 1

    
Input:
<pre>{
  "formData": {
    "prebuilt": "@scope/pre-built-name"
  }
} </pre>

    
    
Output:
<pre>{} </pre>

    
  


### API Links


<table>
  <thead>
    <tr>
      <th>API Name</th>
      <th>API Documentation Link</th>
      <th>API Link Visibility</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Using Pre-Builts in IAP</td>
      <td><a href="https://docs.itential.com/docs/using-pre-builts-in-iap">https://docs.itential.com/docs/using-pre-builts-in-iap</a></td>
      <td>Public</td>
    </tr>    <tr>
      <td>Updating a Pre-Built</td>
      <td><a href="https://docs.itential.com/docs/updating-a-pre-built">https://docs.itential.com/docs/updating-a-pre-built</a></td>
      <td>Public</td>
    </tr>
  </tbody>
</table>


## Support

Please use your Itential Customer Success account if you need support when using **Pre-Built Wizard Re-Discovery**.