{
  "name": "Pre-Built Wizard",
  "webName": "Pre-Built Wizard",
  "type": "Workflow Project",
  "vendor": "Itential",
  "product": "Itential Automation Platform (IAP)",
  "method": "IAP",
  "osVersions": [
    ""
  ],
  "apiVersions": [
    "^6.5.5-2023.2.0"
  ],
  "iapVersions": [
    "2023.2"
  ],
  "domains": [],
  "tags": [],
  "useCases": [],
  "deprecated": {
    "isDeprecated": false
  },
  "brokerSince": "",
  "documentation": {
    "storeLink": "",
    "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-wizard",
    "npmLink": "https://www.npmjs.com/package/@itentialopensource/prebuilt-wizard",
    "docLink": "",
    "demoLinks": [
      {
        "title": "Create Pre-Builts for CI/CD: Bundling Your Automations",
        "link": "https://www.itential.com/resource/demo/create-pre-builts-for-ci-cd-bundling-your-automations/"
      },
      {
        "title": "CI/CD Versioning & Promotion with Itential Pre-Built Automations",
        "link": "https://www.itential.com/resource/demo/ci-cd-versioning-promotion-with-itential-pre-built-automations/"
      }
    ],
    "trainingLinks": [],
    "faqLink": "",
    "contributeLink": "",
    "issueLink": "",
    "webLink": "",
    "vendorLink": "https://www.itential.com/",
    "productLink": "https://www.itential.com/automation-platform/",
    "apiLinks": [
      {
        "title": "Downloading a Pre-Built",
        "link": "https://docs.itential.com/docs/downloading-a-pre-built",
        "public": true
      },
      {
        "title": "Using Pre-Builts in IAP",
        "link": "https://docs.itential.com/docs/using-pre-builts-in-iap",
        "public": true
      },
      {
        "title": "Updating a Pre-Built",
        "link": "https://docs.itential.com/docs/updating-a-pre-built",
        "public": true
      }
    ]
  },
  "assets": [
    {
      "name": "Pre-Built Wizard",
      "webName": "Pre-Built Wizard",
      "assetType": "Workflow",
      "overview": "Pre-Built Wizard provides the ability to create a Pre-Built that is installed into Admin Essentials in IAP made up of IAP components for a particular use case or automation.",
      "iapVersions": [
        "2023.2"
      ],
      "exampleInput": "{\n  \"formData\": {\n    \"readme\": \"# Pre-Built Name\\n\\n## Table of Contents\\n\\n- [Pre-Built Name](#pre-built-name)\\n  - [Table of Contents](#table-of-contents)\\n  - [Overview](#overview)\\n    - [Capabilities](#capabilities)\\n    - [Prerequisites](#prerequisites)\\n    - [How to Install](#how-to-install)\\n    - [Testing](#testing)\\n      - [Adapters](#adapters)\\n      - [External Systems](#external-systems)\\n  - [Using this Pre-Built](#using-this-pre-built)\\n    - [Entry Point IAP Component](#entry-point-iap-component)\\n    - [Inputs](#inputs)\\n    - [Outputs](#outputs)\\n  - [Additional Information](#additional-information)\\n\\n## Overview\\n\\nA high level overview of what the Pre-Built does.\\n\\n### Capabilities\\n\\nA list of capabilities of what the Pre-Built. For example:\\n\\n- Onboard a device to controller\\n- Perform a software upgrade\\n\\n### Prerequisites\\n\\nUsers must satisfy the following prerequisites to install and run this Pre-Built:\\n\\n- Itential Automation Platform\\n  - `2023.2.x`\\n\\n### How to Install\\n\\nTo install this Pre-Built:\\n\\n- Verify that you are running the documented [prerequisites](#prerequisites) in order to install the Pre-Built.\\n\\n- Follow the instructions on the Itential Documentation site for [importing a Pre-Built](https://docs.itential.com/docs/importing-a-prebuilt-cloud).\\n\\n### Testing\\n\\nAny information related to what testing has been done to ensure this Pre-Built runs as expected.\\n\\n### Adapters\\n\\nAny adapters that need to be installed on IAP to run the Pre-Built. Ideally would also include the adapter version here.\\n\\n### External Systems\\n\\nAny external systems needed to run the Pre-Built. Ideally would also include the external system API and/or OS version here.\\n\\n## Using this Pre-Built\\n\\nAny information for how to use this Pre-Built. For instance, if meant to be run in a childJob task of a larger use case, can state this Pre-Built can be run in a [childJob task](https://docs.itential.com/docs/childjob-task-iap-2023-2).\\n\\nIf the Pre-Built is intended to be run from a northbound system using an API call or to be scheduled to run or manully run whenever needed, can state this Pre-Built can be run by the corresponding [Operations Manager automation trigger](https://docs.itential.com/docs/triggers-ops-manager-iap).\\n\\n### Entry Point IAP Component\\n\\nThe name of the IAP component used to start the Pre-Built. This could be a workflow, an Operations Manager automation, a Transformation (JST), or some other IAP component.\\n\\n### Inputs\\n\\nAny information about the inputs to this Pre-Built.\\n\\n### Outputs\\n\\nAny information about the outputs to this Pre-Built.\\n\\n### Additional Information\\n\\nAny additional information related to the Pre-Built.\\n\",\n    \"zeroTouch\": true,\n    \"keywords\": [\n      {\n        \"keyword\": \"test wizard\"\n      }\n    ],\n    \"operationsManagerAutomations\": [\n      {\n        \"name\": \"Pre-Built Wizard\"\n      }\n    ],\n    \"automationWorkflows\": [\n      {\n        \"name\": \"Backup Configuration\"\n      }\n    ],\n    \"scope\": \"@itentialopensource\",\n    \"preBuiltName\": \"iap - iag - test\",\n    \"preBuiltDescription\": \"iap - iag\",\n    \"contributor\": \"Itential\"\n  }\n}",
      "exampleOutput": "{}"
    },
    {
      "name": "Pre-Built Wizard Re-Discovery",
      "webName": "Pre-Built Wizard Re-Discovery",
      "assetType": "Workflow",
      "overview": "Pre-Built Wizard Re-Discovery provides the ability to update an existing Pre-Built in Admin Essentials with any new or removed IAP components for a particular use case or automation",
      "iapVersions": [
        "2023.2"
      ],
      "exampleInput": "{\n  \"formData\": {\n    \"prebuilt\": \"@scope/pre-built-name\"\n  }\n}",
      "exampleOutput": "{}"
    }
  ],
  "relatedItems": {
    "adapters": [],
    "integrations": [],
    "ecosystemApplications": [],
    "workflowProjects": [],
    "transformationProjects": [],
    "exampleProjects": []
  }
}