# Overview 

This contains modular workflows that operate when integrated with the app-artifacts application that can be installed within IAP. Once installed teams will be able to quickly automate common manual tasks such as creating a Pre-Built artifact.json file made up of IAP components, saving a significant amount of time and effort. This library can be used as modular components with other libraries and automations to build comprehensive end-to-end orchestrated workflows with Itential's low-code platform.


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Pre-Built Wizard</td>
      <td>Pre-Built Wizard provides the ability to create a Pre-Built that is installed into Admin Essentials in IAP made up of IAP components for a particular use case or automation.</td>
    </tr>    <tr>
      <td>Pre-Built Wizard Re-Discovery</td>
      <td>Pre-Built Wizard Re-Discovery provides the ability to update an existing Pre-Built in Admin Essentials with any new or removed IAP components for a particular use case or automation</td>
    </tr>
  </tbody>
</table>

For further technical details on how to install and use this Workflow Project, please click the Technical Documentation tab. 
