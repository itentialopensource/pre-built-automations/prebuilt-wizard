
## 2.0.6 [05-09-2024]

Updates drop down form selection for entry point workflows to not show Project scoped workflows current user has access to.

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!58

2024-05-09 18:48:14 +0000

---

## 2.0.5 [05-08-2024]

Udpates README example values across IAP components to be consistent and update link for importing Pre-Built in that README example

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!57

2024-05-08 15:26:59 +0000

---

## 2.0.4 [05-07-2024]

Updates inputs, example input, and apiLinks in documentation

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!55

2024-05-07 14:26:44 +0000

---

## 2.0.3 [05-06-2024]

Updates example input and external dependency in documentation

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!52

2024-05-06 23:08:03 +0000

---

## 2.0.2 [05-06-2024]

Update workflows with new canvas, removes Categories from input, and adds documentation for 2023.2 certification.

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!51

2024-05-06 21:41:00 +0000

---

## 2.0.1 [02-16-2024]

* Updates request body of API call to get workflows to only get name and _id for drop-down input

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!50

---

## 2.0.0 [10-03-2023]

* Removes Automation Catalog references

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!45

---

## 1.0.4 [08-04-2023]

* Uses regex pattern to match all whitespaces for kebab case.

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!40

---

## 1.0.3 [07-27-2023]

* Made README field required in input JSON from

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!38

---

## 1.0.2 [07-24-2023]

* Updates name prebuilt to Pre-Built

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!32

---

## 1.0.1 [07-24-2023]

* Adds Cypress tests and component list

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!30

---

## 1.0.0 [07-24-2023]

* Updates JST to persist scope, updates spacing of Prebuilt Wizard workflow, and converts Prebuilt Wizard Re-Disocver to new canvas.

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!29

---

## 0.0.12 [07-10-2023]

* Adds limit of 999 to OM automations dropdown

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!27

---

## 0.0.11 [07-05-2023]

* Updated JSON Form and Ops Manager with default Readme

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!21

---

## 0.0.10 [06-20-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!4

---

## 0.0.9 [05-12-2022]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!3

---

## 0.0.8 [05-09-2022]

* updated json form to support GC tree injection

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!2

---

## 0.0.7 [05-09-2022]

* Patch/dsup 1256

See merge request itentialopensource/pre-built-automations/prebuilt-wizard!1

---

## 0.0.6 [12-21-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [12-21-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.4 [12-21-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [12-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [12-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n\n\n\n\n\n\n\n\n\n\n\n
