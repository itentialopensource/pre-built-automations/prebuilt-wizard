## Table of Contents 

  - [Getting Started](#getting-started)
    - [Helpful Background Information](#helpful-background-information)
    - [Prerequisites](#prerequisites)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Workflow Project](#using-this-workflow-project)
    - [Pre-Built Wizard Re-Discovery](#pre-built-wizard-re-discovery)
    - [Pre-Built Wizard](#pre-built-wizard)
  - [Additional Information](#additional-information)
    - [Support](#support)

## Getting Started

This section is helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information

Workflows often include logic that varies from business to business. As a result, we often find that our Workflow Projects are more useful as modular components that can be incorporated into a larger process. In addition, they often can add value as a learning tool on how we integrate with other systems and how we do things within the Itential Automation Platform.

While these can be utilized, you may find more value in using them as a starting point to build around.


### Prerequisites

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **Pre-Built Wizard** has been tested with:

- IAP **2023.2**

### External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>App-Artifacts</td>
      <td></td>
      <td>^6.5.5-2023.2.0</td>
    </tr>
  </tbody>
</table>

### Adapters

No adapters required to run this Workflow Project.

### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Example Project.
- Import the Example Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).

###  Testing

Cypress is generally used to test all Itential Example Projects. While Cypress is an opensource tool, at Itential we have internal libraries that have been built around Cypress to allow us to test with a deployed IAP.

When certifying our Example Projects for a release of IAP we run these tests against the particular version of IAP and create a release branch in GitLab. If you do not see the Example Project available in your version of IAP please contact Itential.

While Itential tests this Example Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Example Project into a development/testing environment in which you can test the Example Project.

## Using this Workflow Project
Workflow Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs. 

### <ins>Pre-Built Wizard Re-Discovery</ins>
Pre-Built Wizard Re-Discovery provides the ability to update an existing Pre-Built in Admin Essentials with any new or removed IAP components for a particular use case or automation

Capabilities include:
- Take in the name of the Pre-Built installed in Admin Essentials and update it with any new or removed IAP components for an existing network use case or automation



#### Configuring Dependencies
  
##### App-Artifacts

Users need to install App-Artifacts to be able to run Pre-Built Wizard. If you do not currently have App-Artifacts installed as a service on your node, the .tgz file or "tarball" can be obtained from the <a href='https://registry.aws.itential.com/#browse/browse:app-prebuilt_automations' target='_blank'>Nexus repository</a>. This webpage requires an active account with Itential.

Please use your Itential Customer Success account if you need support for this or do not have credentials for Nexus. After downloading the latest App-Artifacts app, please refer to the instructions included in the App-Artifacts README to install it.
  



#### Running Pre-Built Wizard Re-Discovery to Update A Pre-Built Bundle

If there are components that are no longer included in the Pre-Built that need to be recreated with Pre-Built Wizard Re-Discovery, be sure to remove them when interacting with the first manual task shown during runtime of the job. Further, if there are any components that were manually added to the Pre-Built, ensure that those components are still present in this manual task.

After updating IAP components from which to discover any related IAP components, the discovered IAP components will be shown in a second manual task. On that being accepted, Pre-Built Wizard Re-Discovery will update the existing Pre-Built in Admin Essentials with the newly created Pre-Built.



#### Entry Point IAP Component

The primary IAP component to run **Pre-Built Wizard Re-Discovery** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Pre-Built Wizard Re-Discover</td>
      <td>Operations Manager Automation</td>
    </tr>
  </tbody>
</table>

#### Inputs

The following table lists the inputs for **Pre-Built Wizard Re-Discovery**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>prebuilt</td>
      <td>string</td>
      <td>yes</td>
      <td>Name of an existing Pre-Built installed in Admin Essentials</td>
      <td><pre lang="json">@scope/pre-built-name</pre></td>
    </tr>
  </tbody>
</table>



#### Outputs

There are no outputs for **Pre-Built Wizard Re-Discovery**.


#### Query Output

There are no query output examples for **Pre-Built Wizard Re-Discovery**.




#### Example Inputs and Outputs

  
##### Example 1

    
Input:
<pre>{
  "formData": {
    "prebuilt": "@scope/pre-built-name"
  }
} </pre>

    
    
Output:
<pre>{} </pre>

    
  


#### API Links


<table>
  <thead>
    <tr>
      <th>API Name</th>
      <th>API Documentation Link</th>
      <th>API Link Visibility</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Using Pre-Builts in IAP</td>
      <td><a href="https://docs.itential.com/docs/using-pre-builts-in-iap">https://docs.itential.com/docs/using-pre-builts-in-iap</a></td>
      <td>Public</td>
    </tr>    <tr>
      <td>Updating a Pre-Built</td>
      <td><a href="https://docs.itential.com/docs/updating-a-pre-built">https://docs.itential.com/docs/updating-a-pre-built</a></td>
      <td>Public</td>
    </tr>
  </tbody>
</table>



---
### <ins>Pre-Built Wizard</ins>
Pre-Built Wizard provides the ability to create a Pre-Built that is installed into Admin Essentials in IAP made up of IAP components for a particular use case or automation.

Capabilities include:
- Create and export a Pre-Built made up of IAP components for a particular use case or automation



#### Configuring Dependencies
  
##### App-Artifacts

Users need to install App-Artifacts to be able to run Pre-Built Wizard. If you do not currently have App-Artifacts installed as a service on your node, the .tgz file or "tarball" can be obtained from the <a href='https://registry.aws.itential.com/#browse/browse:app-prebuilt_automations' target='_blank'>Nexus repository</a>. This webpage requires an active account with Itential.

Please use your Itential Customer Success account if you need support for this or do not have credentials for Nexus. After downloading the latest App-Artifacts app, please refer to the instructions included in the App-Artifacts README to install it.
  



#### Running Pre-Built Wizard to Create and Export a Pre-built

Operations Manager automation Pre-Built Wizard allows users to create new Pre-Builts that appear in Admin Essentials from an inputted entry point list of Operations Managers and/or Workflows. Note that the entry point to the bundle for the Pre-Built Wizard Operations Manager automation must be one or more Operations Manager automation(s) or Workflow(s).

After performing discovery, Pre-Built Wizard will display the full list of components that will be used to create the Pre-Built bundle's artifact.json file. If Zero Touch mode is not selected, users have the option to modify this list during the automation.

After Pre-Built Wizard creates the Pre-Built, the Pre-Built will be installed into Admin Essentials of the IAP instance where the job is run and if Zero Touch mode is not selected, a download link for the bundle file will be provided.

If components of this Pre-Built change over time, the `Pre-Built Wizard Re-Discovery` Operations Manager automation can be used to run discovery on the Pre-Built again and update the component list in Admin Essentials.



#### Entry Point IAP Component

The primary IAP component to run **Pre-Built Wizard** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Pre-Built Wizard</td>
      <td>Operations Manager Automation</td>
    </tr>
  </tbody>
</table>

#### Inputs

The following table lists the inputs for **Pre-Built Wizard**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>preBuiltName</td>
      <td>string</td>
      <td>yes</td>
      <td>Name of the Pre-Built to be created</td>
      <td><pre lang="json">Pre-Built Name</pre></td>
    </tr>    <tr>
      <td>preBuiltDescription</td>
      <td>string</td>
      <td>yes</td>
      <td>Description or additional information about the Pre-Built</td>
      <td><pre lang="json">Pre-Built description</pre></td>
    </tr>    <tr>
      <td>scope</td>
      <td>string</td>
      <td>yes</td>
      <td>Pre-Built scope to differentiate Pre-Built created from other Pre-Builts such as those from the Itential open source library.</td>
      <td><pre lang="json">@scope</pre></td>
    </tr>    <tr>
      <td>contributor</td>
      <td>string</td>
      <td>yes</td>
      <td>Contributor of the Pre-Built</td>
      <td><pre lang="json">Contributor</pre></td>
    </tr>    <tr>
      <td>keywords</td>
      <td>array</td>
      <td>no</td>
      <td>Any keywords to include in the artifact.json of the Pre-Built</td>
      <td><pre lang="json">[
  {
    "keyword": "device management"
  }
]</pre></td>
    </tr>    <tr>
      <td>readme</td>
      <td>string</td>
      <td>yes</td>
      <td>README Markdown content that provides any documentation for using the Pre-Built. This content is shown in Admin Essentials when browsing Pre-Builts installed on IAP. An example README is preset in the Create Pre-Built Form</td>
      <td><pre lang="json"># README content</pre></td>
    </tr>    <tr>
      <td>zeroTouch</td>
      <td>boolean</td>
      <td>yes</td>
      <td>If checked or set to true discovered IAP components and options to add or remove any IAP components to the Pre-Built before creating the Pre-Built will be shown. Additionally an option to download the artifact.json of the created Pre-Built will be shown. If left unchecked or set to false none of these messages will be shown.</td>
      <td><pre lang="json">true</pre></td>
    </tr>    <tr>
      <td>automationWorkflows</td>
      <td>array</td>
      <td>yes</td>
      <td>List of workflows to include in the Pre-Built that are also used as entry points to discover any other referenced IAP components to bundle with the Pre-Built</td>
      <td><pre lang="json">[
  {
    "name": "Backup Configuration"
  }
]</pre></td>
    </tr>    <tr>
      <td>operationsManagerAutomations</td>
      <td>array</td>
      <td>yes</td>
      <td>List of Operations Manager Automations to include in the Pre-Built that are also used as entry points to discover any other referenced IAP components to bundle with the Pre-Built</td>
      <td><pre lang="json">[
  {
    "name": "Deploy Device"
  }
]</pre></td>
    </tr>
  </tbody>
</table>



#### Outputs

There are no outputs for **Pre-Built Wizard**.


#### Query Output

There are no query output examples for **Pre-Built Wizard**.




#### Example Inputs and Outputs

  
##### Example 1

    
Input:
<pre>{
  "formData": {
    "readme": "# Pre-Built Name\n\n## Table of Contents\n\n- [Pre-Built Name](#pre-built-name)\n  - [Table of Contents](#table-of-contents)\n  - [Overview](#overview)\n    - [Capabilities](#capabilities)\n    - [Prerequisites](#prerequisites)\n    - [How to Install](#how-to-install)\n    - [Testing](#testing)\n      - [Adapters](#adapters)\n      - [External Systems](#external-systems)\n  - [Using this Pre-Built](#using-this-pre-built)\n    - [Entry Point IAP Component](#entry-point-iap-component)\n    - [Inputs](#inputs)\n    - [Outputs](#outputs)\n  - [Additional Information](#additional-information)\n\n## Overview\n\nA high level overview of what the Pre-Built does.\n\n### Capabilities\n\nA list of capabilities of what the Pre-Built. For example:\n\n- Onboard a device to controller\n- Perform a software upgrade\n\n### Prerequisites\n\nUsers must satisfy the following prerequisites to install and run this Pre-Built:\n\n- Itential Automation Platform\n  - `2023.2.x`\n\n### How to Install\n\nTo install this Pre-Built:\n\n- Verify that you are running the documented [prerequisites](#prerequisites) in order to install the Pre-Built.\n\n- Follow the instructions on the Itential Documentation site for [importing a Pre-Built](https://docs.itential.com/docs/importing-a-prebuilt-cloud).\n\n### Testing\n\nAny information related to what testing has been done to ensure this Pre-Built runs as expected.\n\n### Adapters\n\nAny adapters that need to be installed on IAP to run the Pre-Built. Ideally would also include the adapter version here.\n\n### External Systems\n\nAny external systems needed to run the Pre-Built. Ideally would also include the external system API and/or OS version here.\n\n## Using this Pre-Built\n\nAny information for how to use this Pre-Built. For instance, if meant to be run in a childJob task of a larger use case, can state this Pre-Built can be run in a [childJob task](https://docs.itential.com/docs/childjob-task-iap-2023-2).\n\nIf the Pre-Built is intended to be run from a northbound system using an API call or to be scheduled to run or manully run whenever needed, can state this Pre-Built can be run by the corresponding [Operations Manager automation trigger](https://docs.itential.com/docs/triggers-ops-manager-iap).\n\n### Entry Point IAP Component\n\nThe name of the IAP component used to start the Pre-Built. This could be a workflow, an Operations Manager automation, a Transformation (JST), or some other IAP component.\n\n### Inputs\n\nAny information about the inputs to this Pre-Built.\n\n### Outputs\n\nAny information about the outputs to this Pre-Built.\n\n### Additional Information\n\nAny additional information related to the Pre-Built.\n",
    "zeroTouch": true,
    "keywords": [
      {
        "keyword": "test wizard"
      }
    ],
    "operationsManagerAutomations": [
      {
        "name": "Pre-Built Wizard"
      }
    ],
    "automationWorkflows": [
      {
        "name": "Backup Configuration"
      }
    ],
    "scope": "@itentialopensource",
    "preBuiltName": "iap - iag - test",
    "preBuiltDescription": "iap - iag",
    "contributor": "Itential"
  }
} </pre>

    
    
Output:
<pre>{} </pre>

    
  


#### API Links


<table>
  <thead>
    <tr>
      <th>API Name</th>
      <th>API Documentation Link</th>
      <th>API Link Visibility</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Using Pre-Builts in IAP</td>
      <td><a href="https://docs.itential.com/docs/using-pre-builts-in-iap">https://docs.itential.com/docs/using-pre-builts-in-iap</a></td>
      <td>Public</td>
    </tr>    <tr>
      <td>Downloading a Pre-Built</td>
      <td><a href="https://docs.itential.com/docs/downloading-a-pre-built">https://docs.itential.com/docs/downloading-a-pre-built</a></td>
      <td>Public</td>
    </tr>
  </tbody>
</table>



---
## Additional Information

### Support
Please use your Itential Customer Success account if you need support when using this Workflow Project.



