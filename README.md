# Pre-Built Wizard

## Overview

This contains modular workflows that operate when integrated with the app-artifacts application that can be installed within IAP. Once installed teams will be able to quickly automate common manual tasks such as creating a Pre-Built artifact.json file made up of IAP components, saving a significant amount of time and effort. This library can be used as modular components with other libraries and automations to build comprehensive end-to-end orchestrated workflows with Itential's low-code platform.


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href='https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-wizard/-/blob/master/documentation/Pre-Built Wizard.md' target='_blank'>Pre-Built Wizard</a></td>
      <td>Pre-Built Wizard provides the ability to create a Pre-Built that is installed into Admin Essentials in IAP made up of IAP components for a particular use case or automation.</td>
    </tr>    <tr>
      <td><a href='https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-wizard/-/blob/master/documentation/Pre-Built Wizard Re-Discovery.md' target='_blank'>Pre-Built Wizard Re-Discovery</a></td>
      <td>Pre-Built Wizard Re-Discovery provides the ability to update an existing Pre-Built in Admin Essentials with any new or removed IAP components for a particular use case or automation</td>
    </tr>
  </tbody>
</table>


## External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>App-Artifacts</td>
      <td></td>
      <td>^6.5.5-2023.2.0</td>
    </tr>
  </tbody>
</table>

## Adapters

No adapters required to run this Workflow Project.